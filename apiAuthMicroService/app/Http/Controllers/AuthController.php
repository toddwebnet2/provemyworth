<?php

namespace App\Http\Controllers;

use App\Services\AuthTokenService;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        $user = User::where([
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ]);
        if ($user->count() == 0) {
            throw new \Exception('Invalid credentials.');
        }
        $user = $user->get()->first();
        $token = AuthTokenService::createToken($user->id);
        $return = array_merge($user->toArray(), $token->toArray());
        unset($return['user_id']);
        unset($return['id']);
        return $return;

    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|max:150',
            'name' => 'required|max:150',
            'password' => 'required|min:8|max:32',
        ]);

        $userData = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (User::where('email', $userData['email'])->count() > 0) {
            throw new \Exception('User already exists.');
        }
        $user = new User;
        foreach ($userData as $key => $value) {
            $user->$key = $value;
        }
        $user->save();
        return $user;
    }

    public function index()
    {
        return User::all();
    }

    public function confirm(Request $request)
    {
        $token = $request->get('token');
        return ['isCurrent' => AuthTokenService::isCurrent($token)];
    }
}
