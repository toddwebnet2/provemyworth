<?php

namespace App\Services;

use App\Models\AuthToken;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class AuthTokenService
{
    public static function get($token)
    {
        $token = AuthToken::where('token', $token)->get();
        if ($token) {
            $token = $token->first();
        }
        return $token;
    }

    public static function isCurrent($token)
    {
        $now = Carbon::now();
        $token = self::get($token);
        if (!$token) {
            return false;
        }
        $expireDate = Carbon::createFromTimestamp(strtotime($token->expire_date));
        return ($now->lessThan($expireDate) ) ? true : false;
    }

    public static function createToken($userId)
    {
        $token = new AuthToken();
        $token->token = self::generateToken();
        $token->user_id = $userId;
        $token->expire_date = Carbon::now()->addMinutes(30);
        $token->save();
        return $token;
    }

    private static function generateToken()
    {
        return Uuid::uuid4()->toString();
    }
}