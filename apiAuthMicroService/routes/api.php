<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

// todo break out into route::group
Route::post('login', 'AuthController@login');
Route::post('new', 'AuthController@store');
Route::get('login', 'AuthController@index');
Route::get('confirm', 'AuthController@confirm');

